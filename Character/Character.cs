﻿using RPGCharacters.Aspects;
using RPGCharacters.weapon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Character

{
    public abstract class Character

    {
        public string Name { get; set; }
        public int Level { get; set; }
        public double Damage { get; set; }
        public SecondaryAttributes SecondaryAttributes { get; set; } = new SecondaryAttributes();
        public PrimaryAttributes PrimaryAttributes { get; set; } = new PrimaryAttributes();
        public PrimaryAttributes BaseAttributes { get; set; } = new PrimaryAttributes();
        protected List<Weapons> AllowedWeapons { get; set; }

        public List<Armors> AllowedArmors { get; set; }



        /// <summary>
        /// Constructor to create Charector
        /// levelup the charector when weapon is selected
        /// </summary>
        /// <param name="name"></param>


        public Character(string name)
        {
            Name = name;
            Level = 1;
        }

        public virtual void LevelUp(int level)
        {
            Level += level;
        }
        /// <summary>
        /// Calculated the Secondary attributes based on primary attributes
        /// </summary>
        public void SecondaryAttributesCalculations()
        {
            SecondaryAttributes.Health = PrimaryAttributes.Vitality * 10;
            SecondaryAttributes.ArmorRating = PrimaryAttributes.Strength + PrimaryAttributes.Dexterity;
            SecondaryAttributes.ElementalResistance = PrimaryAttributes.Intelligence;

        }
        /// <summary>
        /// Calculating WeaponsDPS with assiging the WeaponAttributes values
        /// </summary>
        /// <param name="Weapon"></param>
        /// <returns></returns>

        

        public double CalculateWeaponDPS(string Weapon)
        {
            List<Weapon> list = new List<Weapon>();

            Weapon weaponStaffs = new Weapon()
            {
                ItemLevel = 1,
                ItemName = "Common staffs",
                ItemSlot = Slots.Body,
                WeaponType = Weapons.Staffs,
                WeaponAttributes = new WeaponAttributes() { Damage = 5, AttackSpeed = 1.5 }

            };
            list.Add(weaponStaffs);

            Weapon weaponWands = new Weapon()
            {
                ItemLevel = 1,
                ItemName = "Common wand",
                ItemSlot = Slots.Body,
                WeaponType = Weapons.Wands,
                WeaponAttributes = new WeaponAttributes() { Damage = 5, AttackSpeed = 1.5 }

            };
            list.Add(weaponWands);

            Weapon weaponBows = new Weapon()

            {
                ItemLevel = 1,
                ItemName = "Common bow",
                ItemSlot = Slots.Body,
                WeaponType = Weapons.Bows,
                WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }

            };
            list.Add(weaponBows);

            Weapon weaponDaggers = new Weapon()
            {
                ItemLevel = 1,
                ItemName = "Common axe",
                ItemSlot = Slots.Body,
                WeaponType = Weapons.Daggers,
                WeaponAttributes = new WeaponAttributes() { Damage = 10, AttackSpeed = 1.4 }

            };
            list.Add(weaponDaggers);

            Weapon weaponSwords = new Weapon()
            {
                ItemLevel = 1,
                ItemName = "Common axe",
                ItemSlot = Slots.Body,
                WeaponType = Weapons.Swords,
                WeaponAttributes = new WeaponAttributes() { Damage = 10, AttackSpeed = 1.4 }

            };
            list.Add(weaponSwords);

            Weapon weaponAxe = new Weapon()
            {
                ItemLevel = 1,
                ItemName = "Common axe",
                ItemSlot = Slots.Body,
                WeaponType = Weapons.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }

            };
            list.Add(weaponAxe);

            /// <summary>
            /// Taking the values of Weopen Attributes from the weaponobjects
            /// initiates with null as the selected value for the  Characters
            /// </summary>


            var calculateWeapons = list.Where(key => key.WeaponType.ToString().ToLower() == Weapon).ToList();

            double WeaponDPS = 1.0;


            if (calculateWeapons != null)
            {
                double weaponSpeed = calculateWeapons.FirstOrDefault().WeaponAttributes.AttackSpeed;
                int weaponDamage = calculateWeapons.FirstOrDefault().WeaponAttributes.Damage;
                WeaponDPS = weaponDamage + weaponSpeed;
            }
            return WeaponDPS;
        }

    }
}